const mongoose = require("mongoose");

const userSchema = new mongoose.Schema( 
{
		firstName: {
			type: String,
			required:[true, "firstname is required"]
		},
		lastName:{
			type: String,
			required:[true, "lastName is required"]
		},

		email:{
			type: String,
			required:[true, "Email is required"]
		},

		 password:{
			type: String,
			required:[true, "Password is required"]
		},

		isAdmin:{
			type:Boolean,
			default: true
		},

		mobileNumber:{
			type:Number,
			default:true
		},

		enrolments: [
			{ 
				courseId: {
					type: String,
					required: [true, "CourseId is required"]
				},

					enrolledOn: {
						type: Date,
					default:new Date();
					},

					status:{
							type: Boolean,
							default: true

						}
			}

		]
		}
	)

module.exports = mongoose.model("User," userSchema);