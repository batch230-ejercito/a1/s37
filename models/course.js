const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema( 
{
		name: {
			type: String,
			required:[true, "Course is required"]
		},
		description:{
			type: String,
			required:[true, "Description is required"]
		},

		price :{
			type: Number,
			required:[true, "Price is required"]
		},

		isActive:{
			type:Boolean,
			default: true
		},

		createdOn{
			type:Date,
			// The "New date()"expression instantiates a new "Date" that the current date and time whenever a course is created in our database
		default: new Date();
		},

		enrolees: [
			{ 
				userId: {
					type: String,
					required: [true, "UserId is required"]
				},

					enrolledOn: {
						type: Date,
					default:new Date();
					}

					
			}

		]
		}
	)

module.exports = mongoose.model("Course," courseSchema);


// "name": "Information Technology"
// "enrollees": [

// {
// 	"userId": "001",
// 	"enrolledOn": "feb 15, 2022"

// }


// 	]
