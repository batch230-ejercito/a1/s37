/*
		gitbash
		npm init -y
		npm install express
		npm install mongoose
		npm install cors

*/

const express = require ("express");
const mongoose = require ("mongoose");
const cors = require ("cors");

// to create a server / application 
const app = express();
// middlewares - allows to bridge our back end application (server) to our front end
//to allow cross origin resources sharing, si front end pwede na ma gamit
app.use(cors());
// to read json objects 
app.use(express.json());
//to read  forms 
app.use(express.urlencoded({extended:true}));

// connect to our mongo db database
mongoose.connect("mongodb+srv://admin:admin@batch230.5tt6rxu.mongodb.net/courseBooking?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

mongoose.connection.once("open", () =>console.log("Now connected to Ejercito-Mongo DB Atlas"));



app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});